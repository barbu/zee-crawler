# Zee Crawler

A web crawler made to aggregate various databases from french rustic seeds banks.

Notice that it is in an early development state.

## Supported Seed Banks

  * [biaugerme](http://www.biaugerme.com/)
  * [germinance](http://www.germinance.com/)
  * [kokopelli](http://kokopelli-semences.fr/)

## Usage

```
python zee-crawler.py
```

And open your web browser to [127.0.0.1:5000](127.0.0.1:5000).

Login using USERNAME='admin' and PASSWORD='default', you may change these in 'zee-crawler.py'.

Initialize the database first, then udate it.