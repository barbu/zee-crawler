#!/bin/python
# -*- coding: utf-8 -*-

from BeautifulSoup import BeautifulSoup
import re
import urllib
import time
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


def wget(url) :
    resource = urllib.urlopen(url)
    encoding = resource.headers.getheader('Content-Type').split('charset=')[-1]
    if(encoding != 'text/html') :
        return resource.read().decode(encoding)
    else :
        return resource.read().decode()


class Item :

    def __init__(self) :
        self.category = 'No Category'
        self.url = 'about:blank'
        self.name = 'No Name'
        self.prices = {}
        self.text = 'No Description'

    def __repr__(self):
        return "<Item name:%s category:%s text:%s prices:%s url:%s>" % (self.name, self.category, self.text, self.prices, self.url)

    def __str__(self):
        return "<Item name:%s category:%s text:%s prices:%s url:%s>" % (self.name, self.category, self.text, self.prices, self.url)

        
class biaugerme :
    name ="biaugerme"
    url = "http://www.biaugerme.com/"
    
    def __init__(self) :
        self.categories_list = {}
        self.item_list = []
        self.current_item = 0
        self.progress = 0
        self.status = "Crawler Initialized"

    
    def remaining_items(self) :
        return len(self.categories_list.keys()) - self.current_item

    
    def get_categories(self) :
        self.status = "Downloading Category List"
        content = wget(self.url)
        soup = BeautifulSoup(content)

        categorySoup = soup.findAll("a", {"id" : re.compile(r"^lien")})
        for category in categorySoup :
            self.categories_list[category.text] = self.url+category['href']

            
    def get_current_item(self) :
        self.status = "Downloading " + self.categories_list.keys()[self.current_item]
        content = wget(self.categories_list[ self.categories_list.keys()[self.current_item] ])
        soup = BeautifulSoup(content)

        entriesSoup = BeautifulSoup(unicode(soup.find("ul", {"id" : "listevariete"})))
        for entry in entriesSoup.findAll('li', {'class' : 'fiche' }) :
            item          = Item()
            item.name     = entry.find('h2').text
            item.category = self.categories_list.keys()[self.current_item]
            item.url      = self.categories_list[ self.categories_list.keys()[self.current_item] ]

            grammagesSoup = BeautifulSoup(unicode(entry.find('ul', {'class' : 'grammages'})))
            for amount, price in zip(grammagesSoup.findAll('span', {'class' : 'poids'}),
                                     grammagesSoup.findAll('span', {'class' : 'prix'})) :
                item.prices[amount.text.replace('+- ', '+-').replace(',', '.')] = price.text.replace('&nbsp;&euro;', ' €')
                    
            self.item_list.append(item)
                
        self.progress = 100 * self.current_item / len(self.categories_list)
        self.current_item += 1
    
class germinance :
    name ="germinance"
    url = "http://www.germinance.com/"

    def __init__(self) :
        self.categories_list = {}
        self.item_list = [] #self.get_item_list()
        self.current_item = 0
        self.progress = 0
        self.status = "Crawler Initialized"


    def remaining_items(self) :
        return len(self.categories_list.keys()) - self.current_item

        
    def get_categories(self) :
        self.status = "Downloading Category List"
        content = wget(self.url+"catalogue_en_ligne_3.php")
        soup = BeautifulSoup(content)

        categorySoup = BeautifulSoup(unicode(soup.findAll('td', {'class': 'fichetab'}))).findAll('a')
        for category in categorySoup :
            self.categories_list[category.text] = self.url+category['href']

    
    def get_current_item(self) :
        self.status = "Downloading " + self.categories_list.keys()[self.current_item]
        #print unicode(self.progress) + ' ' + self.categories_list[ self.categories_list.keys()[self.current_item] ]
        content = wget(self.categories_list[ self.categories_list.keys()[self.current_item] ])
        soup = BeautifulSoup(content)
        entries = soup.findAll('div', {'class' : re.compile(r"fiche\dboutique\d")})
        for entry in entries :
            item          = Item()
            item.name     = entry.find('p', {'class' : 'boutiquetitre'}).text
            item.category = self.categories_list.keys()[self.current_item]
            item.url      = self.categories_list[ self.categories_list.keys()[self.current_item] ]
            item.text     = entry.find('p', {'class' : 'boutiquetitre'}).findNext('p').text
            for grammages in entry.findAll('div', {'class' : re.compile(r"prix\d")}) :
                i = grammages.find('a', {'href' : re.compile(r"^javascript:ajouter_panier")})
                if(i) :
                    item.prices[ i.text.split(' - ')[0].replace('graines', 'grn') ] = i.text.split(' - ')[1].replace('&euro;', '€')
            self.item_list.append(item)
    
        self.progress = 100 * self.current_item / len(self.categories_list)
        self.current_item += 1


class kokopelli :
    name ="kokopelli"
    url = "http://kokopelli-semences.fr/"

    def __init__(self) :
        self.categories_list = {}
        self.item_list = []
        self.current_item = 0
        self.progress = 0
        self.status = "Crawler Initialized"

    
    def remaining_items(self) :
        return len(self.categories_list.keys()) - self.current_item

    
    def get_categories(self) :
        self.status = "Downloading Category List"
        content = wget(self.url+"boutique")
        soup = BeautifulSoup(content)

        categorySoup = BeautifulSoup(unicode(soup.findAll('div', {'id': 'boutique'}))).findAll('a')
        for category in categorySoup :
            if ( category.text != "Divers" and
                 category.text != "Carte Cadeau"  and
                 category.text != "Autocollants"  and
                 category.text != "Présentoirs"  and
                 category.text != "T-Shirts en Coton Bio"  and
                 category.text != "Thés Militants Kokopelli"  and
                 category.text != "Promotions"  and
                 category.text != "Cadeaux Fertiles"  and
                 category.text != "Collections printanières"  and
                 category.text != "Ouvrages/Calendrier/DVDs"  and
                 category.text != "Apiculture"  and
                 category.text != "Arboriculture"  and
                 category.text != "Autonomie"  and
                 category.text != "Calendriers"  and
                 category.text != "CD Audio"  and
                 category.text != "Connaissance des Plantes"  and
                 category.text != "Cuisine et saveurs"  and
                 category.text != "Divers et Variés"  and
                 category.text != "DVD"  and
                 category.text != "Habitat Écologique"  and
                 category.text != "Jardinage"  and
                 category.text != "Méthode Révolutionnaire"  and
                 category.text != "Ouvrages militants"  and
                 category.text != "Plantes Médicinales"  and
                 category['href'] != "http://kokopelli-semences.fr/boutique/nouveautes" and
                 category['href'] != "http://kokopelli-semences.fr/boutique/protection_de_la_biodiversite" ):
                #print category
                self.categories_list[category.text] = category['href']
        
        
    def get_current_item(self) :
        #content = wget("https://kokopelli-semences.fr/boutique/courge")
        self.status = "Downloading " + self.categories_list.keys()[self.current_item]
        content = wget(self.categories_list[ self.categories_list.keys()[self.current_item] ])
        soup = BeautifulSoup(content)
        pagination = BeautifulSoup(unicode(soup.find('span', {'class' : 'pagination'} ))).findAll('a')
        #print self.categories_list[ self.categories_list.keys()[self.current_item] ]
        #print soup
        if(len(pagination) > 0):
            pagination.pop(-1)
        pagination.append(0)

        for next_url in pagination :
            entries = soup.findAll('div', {'class' : 'produit'})
            for entry in entries :
                item          = Item()
                item.name     = entry.find('span', {'itemprop' : 'name'}).text
                item.category = self.categories_list.keys()[self.current_item]
                if(len(pagination) > 1):
                    item.url  = pagination[-2]['href']
                else :
                    item.url  = self.categories_list[ self.categories_list.keys()[self.current_item] ]
                item.text     = entry.find('p', {'itemprop' : 'description'}).text
                amount        = entry.find('span', {'class' : 'package dark_red'}).text.split(' ')
                if(len(amount) >= 3) :
                    amount_nb     = amount[2]
                    amount_units  = amount[3].replace('graines.', 'grn').replace('grammes.', 'g')
                    amount        = amount_nb + ' ' + amount_units
                else :
                    amount = 1
                price         = entry.find('span', {'itemprop' : 'price'}).text
                currency      = entry.find('span', {'itemprop' : 'priceCurrency'}).text.replace('EUR', '€')
                item.prices[ amount ] = price+' '+currency
                self.item_list.append(item)

            if(next_url != 0) :
                content = wget(next_url['href'])
                soup = BeautifulSoup(content)

        self.progress = 100 * self.current_item / len(self.categories_list)
        self.current_item += 1

