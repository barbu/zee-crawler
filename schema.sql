drop table if exists entries;
create table entries (
  id integer primary key autoincrement,
  title text not null,
  text text not null
);

drop table if exists items;
create table items (
  item_id integer primary key,
  name text not null,
  category text not null,
  url text not null,
  text text not null
);

drop table if exists prices;
create table prices (
  price_id integer primary key autoincrement,
  item_id integer not null,
  amount text not null,
  price text not null
);

drop table if exists categories;
create table categories (
  category_id integer primary key autoincrement,
  name text not null,
  url text not null
);
