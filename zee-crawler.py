# all the imports
import os
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, Response
import crawler
import re
import threading
import time


# create our little application :)
app = Flask(__name__)

# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'zee-crawler.db'),
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

# sites crawler
class Crawler() :

    def __init__(self) :
        self.progress = 0
        self.status = "Idle"
        self.sites = [ crawler.biaugerme(), crawler.germinance(), crawler.kokopelli() ] 

    def update_status(self, site) :
        self.progress = site.progress
        self.status   = '[' + site.name + '] ' + site.status
        
    def crawl(self) :
        item_id = 0
        # SELECT MAX(column_name) FROM table_name;
        progress_site = 1
        progress_item = 0
        for site in self.sites :
            site.get_categories()
            self.update_status(site)
            while (site.remaining_items() > 0) :
                site.get_current_item()
                self.update_status(site)
            self.progress = 100
            self.status = "Done Crawling"
            
            #print "data:0 Updating Database.\n\n"
            self.progress = 0
            self.status = "Updating Database"
            with app.app_context():
                db = get_db()
                for item in site.item_list :
                    db.execute('insert into items (item_id, name, category, url, text) values (?, ?, ?, ?, ?)',
                               [item_id, item.name, item.category, item.url, item.text])
                    self.status = "Updating "+item.name
                    for amount in item.prices :
                        db.execute('insert into prices (item_id, amount, price) values (?, ?, ?)',
                                   [item_id, amount, item.prices[amount]])
                    item_id += 1

                for category in site.categories_list :
                    db.execute('insert into categories (name, url) values (?, ?)',
                               [category, site.categories_list[category]])
                progress_item += 1
                p = progress_site / len(self.sites)
                p = progress_item / len(site.item_list) * 100 * p
                self.progress = p
                #print 'data:' + str(p) + '\n\n'

                db.commit()
                
            progress_site += 1


crawler = Crawler()
    
def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def init_db():
    """Initializes the database."""
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

    
@app.template_filter('strip_chars')
def strip_chars(s):
    return re.sub("\D", "", s)

@app.template_filter('calc_price')
def calc_price(t):
    price  = float(t[0])
    amount = float(t[1].replace('+-', '').replace(',', '.').replace('g', '').replace('grn', '').split('-')[0])#float(re.sub("\D", "", t[1]))
    return float(int(10000 *price/amount) )/ 10000


#@app.cli.command('initdb')
@app.route('/reset-database')
def initdb():
    if not session.get('logged_in'):
        abort(401)
    """Creates the database tables."""
    init_db()
    print('Initialized the database.')
    return render_template('update-list.html', message='Database initialized successfully')


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


@app.route('/add', methods=['POST'])
def add_entry():
    if not session.get('logged_in'):
        abort(401)
    db = get_db()
    db.execute('insert into entries (title, text) values (?, ?)',
               [request.form['title'], request.form['text']])
    db.commit()
    flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_category_selector'))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_category_selector'))


@app.route('/database')
def database_menu():
    return render_template('update-list.html')


@app.route('/update-database')
def update_database():
    if not session.get('logged_in'):
        abort(401)
    threading.Thread(target=crawler.crawl).start()
    return render_template('update-progress.html', message="Updated.")


# Sends data to update the progress bar
@app.route('/update-progress-stream')
def update_database_stream():
    if not session.get('logged_in'):
        abort(401)
    
    def crawl_progress():
        while(crawler.progress < 100) :
            yield "data:" + unicode(crawler.progress) + " " + crawler.status + "\n\n"
            time.sleep(1) # don't want too much data to be streamed
        yield "data:" + unicode(crawler.progress) + " " + crawler.status + "\n\n"

        
    return Response(crawl_progress(), mimetype='text/event-stream')


# Categories list
@app.route('/')
def show_category_selector():
    
    db = get_db()

    cur = db.execute('select distinct name from categories order by name asc')
    categories = cur.fetchall()
    
    return render_template('category_selector.html', categories=categories, form=request.form)


@app.route('/', methods=['POST'])
def show_items():
    category_list=request.form.getlist('category')
    where_cmd = 'where category="'+category_list[0]+'"'
    for i in range(len(category_list)-1) :
        where_cmd += ' or category="'+category_list[i+1]+'"'
    
    db = get_db()
    cur = db.execute('select item_id, name, category, url from items '+where_cmd+' order by name desc')
    items = cur.fetchall()
    
    where_cmd = 'where item_id="' + unicode(items[0][0]) + '"'
    for i in range(len(items)-1) :
        where_cmd += ' or item_id="' + unicode(items[i+1][0]) + '"'
    cur = db.execute('select item_id, amount, price from prices ' + where_cmd + ' order by price_id desc')
    prices = cur.fetchall()
    
    return render_template('show_items.html', items=items, prices=prices, categories=category_list)


if __name__ == '__main__':
    #init_db()
    app.run()
    app.run(threaded=True)
